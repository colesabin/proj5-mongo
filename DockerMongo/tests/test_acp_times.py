"""
Nose tests for acp_times.py
"""

from acp_times import open_time, close_time
import arrow

import nose    # Testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)


def test_rusa_examples():
    """
    Examples given on rusa.org
    """
    time = arrow.now()
    assert open_time(60, 200, time.isoformat()) == time.shift(hours=+1, minutes=+46).isoformat()
    assert open_time(120, 200, time.isoformat()) == time.shift(hours=+3, minutes=+32).isoformat()
    assert open_time(175, 200, time.isoformat()) == time.shift(hours=+5, minutes=+9).isoformat()
    assert open_time(205, 200, time.isoformat()) == time.shift(hours=+5, minutes=+53).isoformat()
    assert close_time(60, 200, time.isoformat()) == time.shift(hours=+4, minutes=+0).isoformat()
    assert close_time(120, 200, time.isoformat()) == time.shift(hours=+8, minutes=+0).isoformat()
    assert close_time(175, 200, time.isoformat()) == time.shift(hours=+11, minutes=+40).isoformat()
    assert close_time(205, 200, time.isoformat()) == time.shift(hours=+13, minutes=+30).isoformat()
    assert open_time(100, 600, time.isoformat()) == time.shift(hours=+2, minutes=+56).isoformat()
    assert open_time(200, 600, time.isoformat()) == time.shift(hours=+5, minutes=+53).isoformat()
    assert open_time(350, 600, time.isoformat()) == time.shift(hours=+10, minutes=+34).isoformat()
    assert open_time(550, 600, time.isoformat()) == time.shift(hours=+17, minutes=+8).isoformat()
    assert close_time(550, 600, time.isoformat()) == time.shift(hours=+36, minutes=+40).isoformat()
    assert close_time(600, 600, time.isoformat()) == time.shift(hours=+40, minutes=+0).isoformat()

def test_oddities():
    """
    Tests based around the fact that under 60km the closing times
    are different. New for 2018!
    """
    time = arrow.now()
    assert close_time(0, 200, time.isoformat()) == time.shift(hours=+1, minutes=+0).isoformat()
    assert close_time(10, 200, time.isoformat()) == time.shift(hours=+1, minutes=+30).isoformat()
    assert close_time(20, 200, time.isoformat()) == time.shift(hours=+2, minutes=+0).isoformat()
    assert close_time(30, 200, time.isoformat()) == time.shift(hours=+2, minutes=+30).isoformat()
    assert close_time(40, 200, time.isoformat()) == time.shift(hours=+3, minutes=+0).isoformat()
    assert close_time(50, 200, time.isoformat()) == time.shift(hours=+3, minutes=+30).isoformat()
    assert close_time(59, 200, time.isoformat()) == time.shift(hours=+3, minutes=+57).isoformat()
    assert close_time(60, 200, time.isoformat()) == time.shift(hours=+4, minutes=+0).isoformat()
    assert close_time(61, 200, time.isoformat()) == time.shift(hours=+4, minutes=+4).isoformat()

def test_borders():
    """
    Testing spots where the rate changes.
    """
    time = arrow.now()
    assert open_time(0, 1000, time.isoformat()) == time.shift(hours=+0, minutes=+0).isoformat()
    assert close_time(0, 1000, time.isoformat()) == time.shift(hours=+1, minutes=+0).isoformat()
    assert open_time(199, 1000, time.isoformat()) == time.shift(hours=+5, minutes=+51).isoformat()
    assert close_time(199, 1000, time.isoformat()) == time.shift(hours=+13, minutes=+16).isoformat()
    assert open_time(200, 1000, time.isoformat()) == time.shift(hours=+5, minutes=+53).isoformat()
    assert close_time(200, 1000, time.isoformat()) == time.shift(hours=+13, minutes=+20).isoformat()
    assert open_time(201, 1000, time.isoformat()) == time.shift(hours=+5, minutes=+55).isoformat()
    assert close_time(201, 1000, time.isoformat()) == time.shift(hours=+13, minutes=+24).isoformat()
    assert open_time(399, 1000, time.isoformat()) == time.shift(hours=+12, minutes=+6).isoformat()
    assert close_time(399, 1000, time.isoformat()) == time.shift(hours=+26, minutes=+36).isoformat()
    assert open_time(400, 1000, time.isoformat()) == time.shift(hours=+12, minutes=+8).isoformat()
    assert close_time(400, 1000, time.isoformat()) == time.shift(hours=+26, minutes=+40).isoformat()
    assert open_time(401, 1000, time.isoformat()) == time.shift(hours=+12, minutes=+10).isoformat()
    assert close_time(401, 1000, time.isoformat()) == time.shift(hours=+26, minutes=+44).isoformat()
    assert open_time(599, 1000, time.isoformat()) == time.shift(hours=+18, minutes=+46).isoformat()
    assert close_time(599, 1000, time.isoformat()) == time.shift(hours=+39, minutes=+56).isoformat()
    assert open_time(600, 1000, time.isoformat()) == time.shift(hours=+18, minutes=+48).isoformat()
    assert close_time(600, 1000, time.isoformat()) == time.shift(hours=+40, minutes=+0).isoformat()
    assert open_time(601, 1000, time.isoformat()) == time.shift(hours=+18, minutes=+50).isoformat()
    assert close_time(601, 1000, time.isoformat()) == time.shift(hours=+40, minutes=+5).isoformat()
    assert open_time(999, 1000, time.isoformat()) == time.shift(hours=+33, minutes=+3).isoformat()
    assert close_time(999, 1000, time.isoformat()) == time.shift(hours=+74, minutes=+55).isoformat()
    assert open_time(1000, 1000, time.isoformat()) == time.shift(hours=+33, minutes=+5).isoformat()
    assert close_time(1000, 1000, time.isoformat()) == time.shift(hours=+75, minutes=+0).isoformat()
    assert open_time(1001, 1000, time.isoformat()) == time.shift(hours=+33, minutes=+5).isoformat()
    assert close_time(1001, 1000, time.isoformat()) == time.shift(hours=+75, minutes=+0).isoformat()
